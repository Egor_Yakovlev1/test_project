def dbscan(X, eps, min_samples):
    """
    Реализация алгоритма DBSCAN.

    Args:
        X (list of tuples): Список точек в пространстве (например, [(x1, y1), (x2, y2), ...]).
        eps (float): Радиус окрестности для определения соседей.
        min_samples (int): Минимальное количество соседей, чтобы точка была ядром.

    :return:
        list of int: Метки кластеров для каждой точки (0 - выбросы).
    """
    n = len(X)
    visited = [False] * n
    labels = [0] * n
    cluster_id = 0

    def region_query(p):
        neighbors = []
        for q in range(n):
            if (X[q][0] - X[p][0])**2 + (X[q][1] - X[p][1])**2 <= eps**2:
                neighbors.append(q)
        return neighbors

    def expand_cluster(p, neighbors):
        labels[p] = cluster_id
        for q in neighbors:
            if not visited[q]:
                visited[q] = True
                q_neighbors = region_query(q)
                if len(q_neighbors) >= min_samples:
                    neighbors.extend(q_neighbors)
            if labels[q] == 0:
                labels[q] = cluster_id

    for p in range(n):
        if not visited[p]:
            visited[p] = True
            neighbors = region_query(p)
            if len(neighbors) >= min_samples:
                cluster_id += 1
                expand_cluster(p, neighbors)

    return labels


# Пример использования
if __name__ == "__main__":
    k = [
        [[[2576.5, 2567.5, 121, 81], [1607.0, 1943.5, 138, 183]], [0.8249640464782715, 0.6696433424949646]],
        [[[2575.5, 2567.5, 121, 81]], [0.8249640464782715]],
        [[[1605.0, 1944.5, 140, 181]], [0.6686277389526367]],
        [[[1607.0, 1946.5, 134, 177], [2577.5, 2567.5, 121, 81]], [0.674665093421936, 0.8249640464782715]],
    ]

    all_centers = []
    for i in k:
        for m in i[0]:
            center = (int(m[0] + m[2] // 2), int(m[1] + m[3] // 2))
            all_centers.append(center)

    eps = 30
    min_samples = 3
    cluster_labels = dbscan(all_centers, eps, min_samples)
    print("Метки кластеров:", cluster_labels)
