import cv2
import numpy as np


# Создаем базовую матрицу 3x3
base_matrix = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

# Указываем размер новой матрицы
rows, cols = 3024, 4032

# Используем повторение, чтобы создать матрицу нужного размера
result_matrix = np.tile(base_matrix, (rows // 3, cols // 3))
result = result_matrix.astype(np.uint8)

res = cv2.imwrite("mask_ssm.jpg", result)

print(result_matrix[1][0:20])
print(result_matrix.shape)
