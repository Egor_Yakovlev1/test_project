import cv2
import numpy as np
import matplotlib.pyplot as plt


# афинные преобразования (сдвиг)
"""
M = [[1, 0, tx]
     [0, 1, ty]]

M - матрица сдвига
tx и ty сдвиг изображения (то что выходит за границы - обрезается)
"""

img = cv2.imread("../images/sudocu.jpeg")
# img = cv2.resize(img, (500, 500),  interpolation=cv2.INTER_CUBIC)

rows, cols, _ = img.shape
# M = np.float32([[1, 0, 100], [0, 1, 50]])
# dst = cv2.warpAffine(img, M, (cols, rows))
#
# cv2.imshow("dst", dst)
# cv2.waitKey(0)

##############################
#   Поворот
##############################
"""
M = [[cos(g), -sin(g)]
     [sin(g), cos(g)]]
     
M - матрица поворота

Вокруг чего будем вращаться:
M = [[a, b, (1-a)*center(x)-b*center(y)]
     [-b, a, b*center(x)+(1-a)*center(y)]]

a = scale * cos(g)
b = scale * sin(g)
"""

# Вычислить матрицу поворота ((корд. центра вращения), угол, масштаб)
# M = cv2.getRotationMatrix2D(((cols-1) / 2.0, (rows - 1) / 2.0), 90, 1)
# dst = cv2.warpAffine(img, M, (cols, rows))
#
# cv2.imshow("dst", dst)
# cv2.waitKey(0)

##############################
#  Афинная трансформация
##############################
"""
Вычисление по 3 точкам

pts 1 - точки в исходном изображении
pts 2 - эти же точки в новом изображении
"""

# pts1 = np.float32([[50, 50], [200, 50], [50, 200]])
# pts2 = np.float32([[10, 100], [200, 50], [100, 250]])
#
# cv2.circle(img, (50, 50), 10, (0, 255, 0), -1)
# cv2.circle(img, (200, 50), 10, (0, 255, 0), -1)
# cv2.circle(img, (50, 200), 10, (0, 255, 0), -1)
#
# cv2.imshow("img", img)
# cv2.waitKey(0)
#
# M = cv2.getAffineTransform(pts1, pts2)
# dst = cv2.warpAffine(img, M, (cols, rows))
#
# plt.subplot(121), plt.imshow(img), plt.title('Input')
# plt.subplot(122), plt.imshow(dst), plt.title('Output')
# plt.show()

##############################
#  Перспективная трансформация
##############################
"""
Почти тоже самое, что и предыдущее, только 4 точки вместо 3.

если нужно выделить какой то фрагмент на изображении.
"""

pts1 = np.float32([[56, 65], [368, 100], [28, 387], [389, 390]])
pts2 = np.float32([[0, 0], [300, 0], [0, 300], [300, 300]])

cv2.circle(img, (56, 65), 10, (0, 255, 0), -1)
cv2.circle(img, (300, 50), 10, (0, 255, 0), -1)
cv2.circle(img, (28, 387), 10, (0, 255, 0), -1)
cv2.circle(img, (389, 390), 10, (0, 255, 0), -1)

cv2.imshow("img", img)
cv2.waitKey(0)

M = cv2.getPerspectiveTransform(pts1, pts2)
dst = cv2.warpPerspective(img, M, (300, 300))

plt.subplot(121), plt.imshow(img), plt.title('Input')
plt.subplot(122), plt.imshow(dst), plt.title('Output')
plt.show()
