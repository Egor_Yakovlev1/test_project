import requests
import configparser


def send_msg_tg():
    config = configparser.ConfigParser()
    config.read("../config.ini")

    bot_token = config["DEFAULT"]["bot_token"]
    chat_id = config["DEFAULT"]["chat_id"]
    text = 'Тестовое сообщение!'

    url = f'https://api.telegram.org/bot{bot_token}/sendMessage?chat_id={chat_id}&text={text}'
    response = requests.get(url)

    print(response.json())


send_msg_tg()
